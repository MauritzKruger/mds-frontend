/**
 * Author: Mauritz Kruger
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  fetchData(year: number): Observable<any> {
    return this.httpClient.get(`${environment.holidayUrl}?action=getHolidaysForYear&country=za&year=${year}`);
  }
}
