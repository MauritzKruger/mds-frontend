/**
 * Author: Mauritz Kruger
 */
import { Component, Input } from '@angular/core';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent {

  @Input() header: string;
  @Input() description: string;
  @Input() faIcon: any;

  constructor() {
  }

}
