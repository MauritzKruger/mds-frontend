/**
 * Author: Mauritz Kruger
 */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faUmbrellaBeach } from '@fortawesome/free-solid-svg-icons';

import * as moment from 'moment';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';

import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('pdfTable', { static: false, read: ElementRef }) pdfTable:ElementRef;

  faIcon = faUmbrellaBeach;
  year: number = moment().year();
  holidays: null | any[] = null;

  yearForm = new FormGroup({
    year: new FormControl(moment().year(), Validators.compose([
      Validators.required,
      Validators.pattern(/^\d{4}$/),
      ])
    )
  });

  constructor(
    private httpService: HttpService
  ) {
  }

  // Initialize Page with Default value(s)
  ngOnInit() {
    this.year = moment().year();
    this.holidays = null;

    this.yearForm = new FormGroup({
      year: new FormControl(moment().year(), Validators.compose([
          Validators.required,
          Validators.pattern(/^\d{4}$/),
        ])
      )
    });
  }

  // Fetch Holiday from API for valid entered year
  fetchHolidays() {
    this.holidays = null;
    this.year = this.yearForm.value.year;

    // Submit request to API
    this.httpService.fetchData(this.year)
      .subscribe((res) => {
        const dataRes = res;

        if (dataRes?.length > 0) {
          this.holidays = [];

          for (const data of dataRes) {
            const formattedDate = moment([data.date.year, data.date.month - 1, data.date.day]).format('YYYY/MM/DD');
            const dayDescr = (data.name[0]).text;

            this.holidays.push({date: formattedDate, name: dayDescr});
          }
        }
      }, (err) => {
        console.error('HTTP request encountered an error ::', err);
      });
  }

  downloadAsPDF() {
    html2canvas(this.pdfTable.nativeElement).then(canvas => {
      let imgWidth = 200;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png')

      let pdf = new jspdf('p', 'mm', 'a4');
      let position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('South African Holidays.pdf');
    });
  }
}
