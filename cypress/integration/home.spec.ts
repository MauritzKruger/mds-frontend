describe('Home Component Spec', () => {
  it('should ensure that page loads', () => {
    cy.visit('/')
    cy.contains('Angular + Bootstrap')
  })

  it('should check page heading & description', () => {
    cy.get('#pageHeader').contains('South African Holiday(s)')
    cy.get('#pageHeader').contains('Retrieve all South African Holiday(s) for Specified Year')
  })

  it('should have an input for year number with validation' ,() => {
    cy.get('#yearInput').should('be.visible')

    cy.get('#yearInput').clear({force: true}).type('text').should('have.value', 'text').blur()
    cy.get('#fetchButton').should('be.disabled')

    cy.get('#yearInput').clear({force: true}).should('have.value', '').blur()
    cy.get('#fetchButton').should('be.disabled')

    cy.get('#yearInput').clear({force: true}).type('2020').should('have.value', '2020').blur()
    cy.get('#fetchButton').should('be.enabled')
  })

  it('should fetch a list of holidays for South Africa' ,() => {
    cy.get('#fetchButton').click({force: true})
    cy.wait(6000)

    // Count table rows - 1 heading, 13 holidays
    cy.get('#pdfTable').find('tr').should('have.length', 14)
  })

  it('should have a button to export list of holidays as PDF' ,() => {
    cy.get('#pdfExportButton').should('be.visible')

    cy.get('#pdfExportButton').should('be.enabled')
  })
})
