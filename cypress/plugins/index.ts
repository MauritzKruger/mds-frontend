/// <reference types="cypress" />
/**
 * Cypress - Plugin(s)
 *
 * @help  :: See https://on.cypress.io/plugins-guide
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
};
