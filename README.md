### Welcome to the MDS Technologies
#### South African Holidays Webpage

Clone repo into any directory, once cloned, navigate into the newly cloned directory.
Once in the directory, run command `npm i` to install project node modules.

#### To launch environment
Run `ng serve` for server. Navigate to `http://localhost:4200/`.

#### To launch e2e tests
Run `npm run e2e` to launch cypress tests. On launched window, each spec file can be run to see which tests pass and which ones fail.

#### Additioinal Details
Should any additional information be required, please do not hesitate to get in contact with me.
